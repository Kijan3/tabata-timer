# Tabata Timer
> Timer that helps you perform Tabata exercises correctly and save your achievements.
## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)

## General info
Tabata involved 20-second bursts of very intense exercise followed by ten seconds of rest, repeated eight times for a total of four minutes. 
This application contains a timer that can be paused or restarted at any time. In the settings you can change the time of exercise, pause and number of repetitions. The last element is the calendar in which the number of exercises performed on a given day is recorded.

## Screenshots
![Home screenshot](app/ApplicationScreenshots/Home.png)
![Exercise screenshot](app/ApplicationScreenshots/Start.png)
![Exercise training screenshot](app/ApplicationScreenshots/Train.png)
![Exercise rest screenshot](app/ApplicationScreenshots/Rest.png)
![Settings screenshot](app/ApplicationScreenshots/Settings.png)
![Callendar screenshot](app/ApplicationScreenshots/Callendar.png)


## Technologies
* Android Studio 3.4.1
* JRE: 1.8.0_152
* JVM: OpenJDK 

## Features
The application consists of 4 fragments
* Home - home page
* Timer - after pressing play, training starts, alternately counting down the time for exercising and resting. Training can be paused or restarted. During the exercise the background of the screen is red and during rest green. The screen displays information about the training status and the number of repetitions remaining to the end.
* Settings - you can change the time of exercise, rest and choose the number of repetitions. After clicking "Zapisz zmiany" -  the changes will be saved. after pressing "Anuluj" settings will be restored to previous values.
* Calendar - After completing your workout, the information is saved, a barbell icon appears on the calendar, as well as the number of workouts completed on a given day. By default, the current month is displayed. Use the left / right arrows to change the months in the calendar view.

To-do list:
* Adding vibrations of different frequency when changing the countdown from exercise to rest as well as after the end of the series.

## Status
Project is: _in progress_
## Contact
Created by MKijanski.

