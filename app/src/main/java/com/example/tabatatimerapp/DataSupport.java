package com.example.tabatatimerapp;

public class DataSupport {

    public String properDateOnTheScreen(String date) {
        String[] arrayOfDateStrings = date.split(" ");

        switch (arrayOfDateStrings[0]) {
            case "stycznia":
                arrayOfDateStrings[0] = "styczeń";
                break;
            case "lutego":
                arrayOfDateStrings[0] = "luty";
                break;
            case "marca":
                arrayOfDateStrings[0] = "marzec";
                break;
            case "kwietnia":
                arrayOfDateStrings[0] = "kwiecień";
                break;
            case "maja":
                arrayOfDateStrings[0] = "maj";
                break;
            case "czerwca":
                arrayOfDateStrings[0] = "czerwiec";
                break;
            case "lipca":
                arrayOfDateStrings[0] = "lipiec";
                break;
            case "sierpnia":
                arrayOfDateStrings[0] = "Sierpień";
                break;
            case "września":
                arrayOfDateStrings[0] = "wrzesień";
                break;
            case "października":
                arrayOfDateStrings[0] = "pażdziernik";
                break;
            case "listopada":
                arrayOfDateStrings[0] = "listopad";
                break;
            case "grudnia":
                arrayOfDateStrings[0] = "grudzień";
                break;
            default:
                arrayOfDateStrings[0] = "miesiąc";
        }
        return arrayOfDateStrings[0] + " " + arrayOfDateStrings[1];
    }
}
